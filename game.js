/**
 * Dodge the walls using WAD for movement.
 * 
 * Credit to Mary Rose - https://vimeo.com/105955605
 * for the walkthrough
 */
(function() {
	var Game = function(canvasId) {
		var canvas = document.getElementById(canvasId);
		var screen = canvas.getContext('2d');
		this.screen = screen;
		var gameSize = {x: canvas.width, y: canvas.height};
		this.gameSize = gameSize;

		screen.font = '10px Consolas';

		this.bodies = createWalls(this, gameSize).concat(new Player(this, gameSize));
		this.scoring = {
			score: 0
		}
		this.gameFinished = false;

		var self = this;
		var tick = function() {
			self.update();
			self.draw(screen, gameSize);
			
			if (!self.gameFinished) {
				requestAnimationFrame(tick);
			}
		}
		tick();
	};

	Game.prototype = {
		update: function() {
			for (var i = this.bodies.length - 1; i >= 0; i--) {
				this.bodies[i].update();
			};
		},
		draw: function(screen, gameSize) {
			screen.clearRect(0, 0, gameSize.x, gameSize.y);
			for (var i = this.bodies.length - 1; i >= 0; i--) {
				drawRect(screen, this.bodies[i]);
			};
			screen.fillStyle = "rgb(0,0,0)";
			screen.fillText('Score: ' + this.scoring.score, 15, 15);
		},
		gameOver: function() {
			//this.screen.fillStyle = "rgba(0, 0, 0, 0.4)";
			this.screen.fillRect(0, 0, this.gameSize.x, this.gameSize.y);
			//this.screen.fillStyle = "rgb(0, 0, 0)";
			//this.screen.fillText("Game Over \n Final Score: " + this.scoring.score, this.gameSize.x/2-50, this.gameSize.y/2-50);
			this.gameFinished = true;
		}
	};

	var Player = function(game, gameSize) {
		this.game = game;
		this.gameSize = gameSize;
		this.size = {x: 15, y: 15};
		this.center = {x: gameSize.x / 2, y: gameSize.y/2 - this.size.x};
		this.keyboarder = new Keyboarder();
		this.velocity = {x: 0, y: 0, max: 5};
		this.color = {r: 0, g: 0, b: 0}
	}

	Player.prototype = {
		update: function() {
			if (this.isColliding()) {
				this.game.gameOver();
			};

			if (this.keyboarder.isDown(this.keyboarder.KEYS.LEFT)) {
				this.center.x -= 2;
			} else if (this.keyboarder.isDown(this.keyboarder.KEYS.RIGHT)) {
				this.center.x += 2;
			};
			if (this.velocity.y < 5 && this.keyboarder.isDown(this.keyboarder.KEYS.UP)) {
				this.velocity.y += 0.30;
			} else if(!this.keyboarder.isDown(this.keyboarder.KEYS.UP) && this.velocity.y > -15) {
				this.velocity.y -= 0.15
			};
			this.color.r = parseInt(Math.abs(this.velocity.y) / 15 * 255);
			this.center.y -= this.velocity.y;
			if (this.center.y + this.size.y/2 < 0) { 
				this.center.y += this.gameSize.y + this.size.y;
			} else if (this.center.y - this.size.y/2 > this.gameSize.y) {
				this.center.y -= this.gameSize.y + this.size.y;
				this.game.scoring.score += 1;
			};
		},
		isColliding: function() {
			var bodies = this.game.bodies;
			var self = this;
			return bodies.filter(function(b2) {
				return colliding(self, b2);
			}).length !== 0;
		}
	};

	var Wall = function(game, gameSize, center) {
		this.game = game;
		this.gameSize = gameSize;
		this.size = {x: gameSize.x/2, y: (gameSize.y /5) - 2};
		this.center = center;
		this.color = {r: 180, g: 100, b: 120};
		this.moving = false;
		this.moved = false;
		this.waitUntil = 0;
		this.velocity = {x: 0, y: 0};
		this.direction = this.center.x > gameSize.x/2 ? -1 : 1;
		this.stopPoint = 0.5;
	}

	Wall.prototype = {
		update: function() {
			if (!this.moving) {
				this.color.b = 120;
				this.moving = Math.random() > 0.9995;
				if (this.moving) {
					this.color.b = 255;
					this.waitUntil = Date.now() + (Math.random() * 1500);
					this.stopPoint = Math.random() * 0.25 + 0.1;
				};
			} else if (this.moved && Date.now() > this.waitUntil) {
				this.moveBack();
			} else if (Date.now() > this.waitUntil) {
				this.moveWall();
			};
		},
		moveWall: function() {
			if (this.direction == -1 && this.center.x > this.gameSize.x*(1 - this.stopPoint)) {
				this.velocity.x += 0.20;
				this.center.x -= this.velocity.x;
			} else if (this.direction == 1 && this.center.x < this.gameSize.x*(0+this.stopPoint)) {
				this.velocity.x += 0.20;
				this.center.x += this.velocity.x;
			} else {
				this.moved = true;
				this.velocity.x = 0;
				this.waitUntil = Date.now() + (Math.random() * 3000);
			};
		},
		moveBack: function() {
			if (this.direction == -1 && this.center.x < this.gameSize.x) {
				this.velocity.x += 0.20;
				this.center.x += this.velocity.x;
			} else if (this.direction == 1 && this.center.x > 0) {
				this.velocity.x += 0.20;
				this.center.x -= this.velocity.x;
			} else {
				this.moved = false;
				this.moving = false;
			};
		}
	}

	var createWalls = function(game, gameSize) {
		var walls = [];
		for (var i = 0; i < 10; i++) {
			var x = (i % 2 === 0) ? 0 : gameSize.x;
			var y = ((i % 5) * (gameSize.y / 5)) + (gameSize.y/10);
			walls.push(new Wall(game, gameSize, {x: x, y: y}));
		};
		return walls;
	}

	var drawRect = function(screen, body) {
		screen.fillStyle = "rgb(" +body.color.r+","+body.color.g+","+body.color.b+")";
		screen.fillRect(body.center.x - body.size.x/2,
				body.center.y - body.size.y/2,
				body.size.x, body.size.y);
	}

	var Keyboarder = function() {
		var keyState = {};

		window.onkeydown = function(e) {
			keyState[e.keyCode] = true;
		};
		window.onkeyup = function(e) {
			keyState[e.keyCode] = false;
		};

		this.isDown = function(keyCode) {
			return keyState[keyCode] === true;
		}

		this.KEYS = {
			LEFT: 65,
			RIGHT: 68,
			UP: 87
		}
	};

	var colliding = function(b1, b2) {
		return !(
				b1 === b2 ||
				b1.center.x + b1.size.x /2 < b2.center.x - b2.size.x / 2 ||
				b1.center.y + b1.size.y /2 < b2.center.y - b2.size.y / 2 ||
				b1.center.x - b1.size.x /2 > b2.center.x + b2.size.x / 2 ||
				b1.center.y - b1.size.y /2 > b2.center.y + b2.size.y / 2
			);
	};

	window.onload = function() {
		new Game('screen');
	};
})();